We (re)centralize all python stuffs related to killerdroid and beyond:

Layout:

- utils:
    + actions on an apk file (sign, get some metadatas ...)
    + actions on a remote devices (install, desinstall, get some properties)
- io: abstraction of a store of apks (biased towards killerdroid use case)
- screenshot:
    + take screenshots
    + compare screen with a reference screenshot
- publisher:
    + publish tasks on a queue (biased towards killerdroid use case)
      used for g5k/igrida experiments

Concepts
--------

- Operation: transformation made in the context of killerdroïd
    + e.g the packer transform 2 apks into a new one given a configutation
- Store: abstract storage for input/outputs of operations
    + FsStore stores the states in a filesystem (one specifies a directory in
      the filesystem). The directory layout inside this store is private.
    + MongoStore stores the states in a Mongo database. We leverage
      Mongoengine (ORM like).


Some entry points
------------------

Note that we use intensively environnement variables to control the behaviour
of the various operation. Sourcing an environment file is desired to lighten
the command line.

- Populate the store with some (raw) apks.

.. code-block:: bash

    STORE=fs FS_STORE_DIRECTORY=packer-store kd-populate -r ../datasets/couple

- Launching packer on this store (the entrypoint in the packer project is store-aware)

.. code-block:: bash

    # generate some params on the queue
    kd-publish -b f11f862f6962bf000fae4b8da4dd9989b32d48852cca7d21d18c141d162cfc7a -m fe666e209e094968d3178ecf0cf817164c26d5501ed3cd9a80da786a4a3f3dc4 -e sweep --limit 1

    # go minion, go !
    ANDROID_HOME=~/workspace/tools/Android/sdk PATH=$PATH:~/workspace/tools/Android/sdk/build-tools/28.0.3/ MINON_ENGINE=execo python entrypoint.py

.. note::

    The entrypoint will upload the resulting packed application to the store automatically

- Getting some screenshots (this one needs some caution as it requires to
  manually check the screenshots)

.. code-block:: bash

    # will iterate on all the apk available in the store
    # so you need to set the relevant env var
    kd-screenshot take

    # will take some screenshot for the passed apk (apk must be populated in the store first)
    kd-screenshot take sha256_1 sha256_2

- Computing the similarity for a packed application

.. code-block:: bash

    # will iterate on all the PACKED apk available in the store
    # so you need to set the relevant env var
    kd-screenshot validate

    # will validate (using structural similariry) the passed apps
    kd-screenshot validate sha256_1 sha256_2


Docker
------

The entrypoint is set to wait on a queue for some `sha256` to validate.
This uses the minionize tool.

.. code-block:: bash

    # build the docker
    cd docker
    docker build -t kdp_screenshot:latest .

    # create the queue (the apk sha256 must be in the store)
    python -c "from execo_engine.sweep import ParamSweeper; ParamSweeper('sweeps', sweeps=['994493f050760c4fdfa135f0f17b9ee14107d061be934e4ee16a581d709c098a'], save_sweeps=True)"

    # Launch the docker (assuming the packer-store is an FsStore located in the current directory)
     docker run -ti --device /dev/kvm \
         -v $ANDROID_HOME/build-tools:/android/sdk/build-tools \
         -e MINION_ENGINE=execo \
         -v $(pwd)/sweeps:/app/sweeps \
         -v $(pwd)/packer-store:/app/packer-store \
         -e FS_STORE_DIRECTORY=/app/packer-store \
         kdp_screenshot:latest
import logging
from typing import Callable

from killerdroid.screenshot import _validate
from killerdroid.utils import wait_for_device
from minionize import FuncCallback, minionizer
from minionize.types import Param

logging.basicConfig(level=logging.DEBUG)


def m_validate(param: Param):
    # param = {"sha256": sha256}
    logging.debug(param)
    _validate([param["sha256"]])


minionized_validate = FuncCallback(m_validate)

# wait here for the device.
# the intent is to prevent any param to be consumed before we're sure that the
# emulator is ok
wait_for_device()
m = minionizer(minionized_validate)
m.run()

#! /bin/bash
set -x

# Launch the minionize python util script alongside an emulator
screen -dm bash -c "cd /android/sdk/ && ./launch-emulator.sh"
cd /app
python3.8 ./screenshot.py "$@"

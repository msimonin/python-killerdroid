"""
Taken from lgitzing code
https://gitlab.inria.fr/lgitzing/killerdroid_core/-/blob/master/killerdroid_core/configuration_manager.py

NOTE(msimonin): 09/08/20 don't know the internals here
"""
import re
import os
import logging
import argparse
import itertools
import random
import json
from pathlib import Path


DIRECTORY = Path(__file__).parent.resolve()
SCHEMA_FILE = DIRECTORY / "killerdroid_configuration_schema.json"


def get_schema_data(filepath):
    if os.path.exists(filepath):
        with open(filepath, "r") as f:
            return json.load(f)
    raise FileNotFoundError(f"file: {filepath} not found")


def flat(elements):
    to_return = []
    master_dict = {}
    for e in elements:
        if isinstance(e, dict):
            for k, v in e.items():
                master_dict[k] = v
        else:
            to_return.extend(flat(e))
    if master_dict:
        to_return.append(master_dict)
    return to_return


def is_list_of_dict(l):
    for i in l:
        if not isinstance(i, dict):
            return False
    return True


def make_product(list1, list2):
    elements = []
    for element in itertools.product(list1, list2):
        l = []
        [l.append(e) for e in element]
        elements.append(l)
    flattened = flat(elements)
    return flattened


def merge_sub_confs(current_configurations, sub_conf):
    if is_list_of_dict(sub_conf):
        if not current_configurations:
            current_configurations.extend(sub_conf)
        else:
            current_configurations = make_product(current_configurations, sub_conf)
    else:
        flattened = []
        for item in sub_conf:
            cflat = merge_sub_confs(current_configurations, item)
            flattened.extend(cflat)
        current_configurations = flattened
    return current_configurations


def generate_all_configs(schema):
    configurations = []
    if "properties" in schema and "select" not in schema:
        sub_configurations = []
        for k, v in schema["properties"].items():
            sub_conf = generate_all_configs(v)
            sub_configurations = merge_sub_confs(sub_configurations, sub_conf)
        titled = []
        for f in sub_configurations:
            titled.append({schema["title"]: f})

        return titled
    elif "select" in schema:
        if schema["select"] == "one":
            sub_confs = []
            for k, v in schema["properties"].items():
                titled = []
                ss = generate_all_configs(v)
                for f in ss:
                    titled.append({schema["title"]: f})
                sub_confs.append(titled)
            # print('---sub conf ---')
            # pprint(sub_confs)
            # print('---end sub conf ---')
            return sub_confs
    else:
        mini_confs = []
        for e in schema["enum"]:
            mini_confs.append({schema["title"]: e})
        return mini_confs

        # for element in itertools.product(*ge):


def get_vector_model(schema):
    vector = []
    path = [schema["title"]]
    if "enum" in schema:
        for e in schema["enum"]:
            vector.append(
                {
                    "title": schema["title"],
                    "path": [],
                    "string_value": str(e),
                    "value": 0,
                }
            )
    else:
        if "properties" in schema:
            k = "properties"
        else:
            k = "items"
        for k, v in schema[k].items():
            sub_vector = get_vector_model(v)
            for i in sub_vector:
                vector.append(i)
                for pp in path:
                    i["path"].insert(0, pp)

    master_set = set()
    for i, item in enumerate(vector):
        string = ""
        for sub_path in item["path"]:
            if sub_path != "configuration":
                string = string + sub_path[0]
        string = string + item["title"][0]
        string = string + item["string_value"][0]
        j = 1
        while string in master_set:
            string = string + item["string_value"][j]
        # if string in master_set:
        #     string = string + str(i)
        # raise RuntimeError(f'identifier already exists: {string}')
        master_set.add(string)
        item["id"] = string
    return vector


def define_identifiers(vector_model):
    sets = {}
    master_set = set()

    for i, item in enumerate(vector_model):
        string = ""
        for sub_path in item["path"]:
            if sub_path != "configuration":
                string = string + sub_path[0]
        string = string + item["title"][0]
        string = string + item["string_value"][0]
        if string in master_set:
            string = string + str(i)
            # raise RuntimeError(f'identifier already exists: {string}')
        master_set.add(string)
        item["id"] = string
    return vector_model


def get_vector(model, config):
    string = ""
    for item in model:
        current = config[item["path"][0]]
        i = 1
        while current and i < len(item["path"]):
            current = current.get(item["path"][i], None)
            i += 1
        if current:
            current = current[item["title"]]
            if str(current) == item["string_value"]:
                string += item["id"].capitalize()
    return string


def get_config_from_vector(model, vector):
    configuration = {}
    split = re.split("([A-Z][^A-Z]*)", vector)
    split = list(filter(None, split))
    for chunk in split:
        for m in model:
            if chunk.lower() == m["id"].lower():
                current = configuration
                for p in m["path"]:
                    if p in current:
                        current = current.get(p)
                    else:
                        current[p] = {}
                        current = current[p]
                try:
                    int(m["string_value"])
                    current[m["title"]] = int(m["string_value"])
                except ValueError:
                    current[m["title"]] = m["string_value"]
    return configuration


def get_all_nodes(vector_model):
    all_nodes = []
    for sub_vector in vector_model:
        all_nodes.append(sub_vector["id"].capitalize())
    return all_nodes


def get_grouped_nodes(vector_model):
    grouped_nodes = {}
    for sub_vector in vector_model:
        title = "_".join(sub_vector["path"][1:] + [sub_vector["title"]])
        group = grouped_nodes.get(title, [])
        group.append(sub_vector["id"].capitalize())
        grouped_nodes[title] = group
    return grouped_nodes


def get_node_details(vector_model):
    nodes = {}
    for sub_vector in vector_model:
        pk = sub_vector["id"].capitalize()
        last_item = sub_vector["path"][-1]
        if last_item != "configuration":
            to_add = "_" + last_item
        else:
            to_add = "_"
        data = {
            "id": pk,
            "group": sub_vector["title"] + to_add,
            "value": sub_vector["string_value"],
            "name": f"{sub_vector['title'] + to_add} {sub_vector['string_value']} ({pk})",
            "name_display": f"{sub_vector['title'] + to_add} {sub_vector['string_value']} ({pk})".replace(
                "_", "\n"
            ).replace(
                " ", "\n"
            ),
        }
        nodes[pk] = data
    return nodes


def get_configuration_data(filepath):
    data = {}

    data["schema_file"] = filepath
    schema_data = get_schema_data(filepath)
    data["schema_data"] = schema_data
    configurations = generate_all_configs(data["schema_data"])
    data["configurations"] = configurations
    vector_model = get_vector_model(data["schema_data"])
    data["vector_model"] = vector_model
    orig_config_strings = set()
    for c in configurations:
        orig_config_strings.add(json.dumps(c))
    all_vectors = set()
    for c in configurations:
        vector = get_vector(vector_model, c)
        if vector in all_vectors:
            raise RuntimeError("Vector already exists !")
        else:
            all_vectors.add(vector)
    for vector in all_vectors:
        conf = get_config_from_vector(vector_model, vector)
        if json.dumps(conf) not in orig_config_strings:
            raise RuntimeError(f"Missing reconstructed conf from vector: {vector}")
    data["vectors"] = all_vectors
    data["nodes"] = get_all_nodes(vector_model)
    data["grouped_nodes"] = get_grouped_nodes(vector_model)
    data["node_details"] = get_node_details(vector_model)
    return data


def get_random_tactic(args, tactic_data):
    print(json.dumps(random.choice(tactic_data["configurations"])["configuration"]))


def hash_to_json(args, tactic_data):
    tactic_dic = get_config_from_vector(tactic_data["vector_model"], args.hash)
    if args.hash not in tactic_data["vectors"]:
        raise ValueError(f"Invalid hash provided: {args.hash}")
    if tactic_dic:
        print(json.dumps(tactic_dic["configuration"]))


def json_to_hash(args, tactic_data):
    with open(args.filepath) as file:
        data = json.load(file)
    full_data = {"configuration": data}
    vector = get_vector(tactic_data["vector_model"], full_data)
    if vector not in tactic_data["vectors"]:
        raise ValueError(f"Invalid json obfuscation provided: {args.filepath}")
    print(vector)


def print_all_hashes(args, tactic_data):
    for v in tactic_data["vectors"]:
        print(v)


def all_confs():
    # NOTE(msimonin): this is suboptimal since we load everything at first but
    # this isn't too intrusive for now
    tactic_data = get_configuration_data(str(SCHEMA_FILE))
    return [
        (get_vector(tactic_data["vector_model"], t), t["configuration"])
        for t in tactic_data["configurations"]
    ]


def lib_output_all(output_dir, tactic_data):
    if not os.path.exists(output_dir):
        os.mkdir(output_dir)
    for t in tactic_data["configurations"]:
        vector = get_vector(tactic_data["vector_model"], t)
        filepath = os.path.join(output_dir, vector + ".json")
        with open(filepath, "w") as file:
            file.write(json.dumps(t["configuration"]))


def output_all(args, tactic_data):
    lib_output_all(args.output_dir, tactic_data)


def main():
    logging.basicConfig(level=logging.INFO)
    parser = argparse.ArgumentParser(
        description="A helper CLI to manage KillerDroid obfuscation tactics"
    )
    parser.add_argument("--schema", dest="schema", metavar="s", type=str, required=True)
    subparsers = parser.add_subparsers()

    get_random_parser = subparsers.add_parser(
        "random", help="output a random obfuscation tactic"
    )
    get_random_parser.set_defaults(func=get_random_tactic)

    hash_to_json_parser = subparsers.add_parser(
        "hash-to-json",
        help="output the obfuscation json configuration corresponding to the provided hash",
    )
    hash_to_json_parser.add_argument(
        "--hash", "-a", type=str, required=True, help="hash version of an obfuscation"
    )
    hash_to_json_parser.set_defaults(func=hash_to_json)

    json_to_hash_parser = subparsers.add_parser(
        "json-to-hash",
        help="output the obfuscation hash corresponding to the provided jsonn configuration",
    )
    json_to_hash_parser.add_argument(
        "--filepath", "-f", type=str, required=True, help="filepath to json file"
    )
    json_to_hash_parser.set_defaults(func=json_to_hash)

    print_all_hash_parser = subparsers.add_parser(
        "print-all-hashes", help="output all hashes"
    )
    print_all_hash_parser.set_defaults(func=print_all_hashes)

    output_all_obfuscations = subparsers.add_parser(
        "output-all", help="output all obfuscation combinations to files"
    )
    output_all_obfuscations.add_argument(
        "--output-dir", "-o", type=str, required=True, help="output directory"
    )
    output_all_obfuscations.set_defaults(func=output_all)

    args = parser.parse_args()

    tactic_data = get_configuration_data(args.schema)
    if hasattr(args, "func"):
        args.func(args, tactic_data)
    else:
        parser.print_help()

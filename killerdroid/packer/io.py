from abc import ABC, abstractmethod, abstractclassmethod
import argparse
import json
import logging
import os
from pathlib import Path
import threading
from typing import Callable, Dict, Optional, IO, Generator, Iterable

from ..models import Apk, PackerApk, to_sha256


_store_lock = threading.Lock()
# Keep track of the store (singleton)
_store = None

LOGGER = logging.getLogger(__name__)

ENV_PACKER_STORE = "PACKER_STORE"

ENV_MONGO_STORE_CACHE_DIR = "MONGO_STORE_CACHE_DIR"
DEFAULT_MONGO_STORE_CACHE_DIR = Path("/tmp/packer-cache")
ENV_MONGO_STORE_CONNECTION = "MONGO_STORE_CONNECTION"

ENV_FS_STORE_DIRECTORY = "FS_STORE_DIRECTORY"
DEFAULT_FS_STORE_DIRECTORY = Path("packer-store")

Sha256 = str


class Store(ABC):
    @abstractmethod
    def iter(self) -> Generator[str, None, None]:
        pass

    @abstractmethod
    def pull(
        self, sha256: str, output_dir: Path = Path.cwd(), force: Optional[bool] = False
    ):
        pass

    @abstractmethod
    def save_apk(self, name: str, content: IO) -> Sha256:
        pass

    @abstractmethod
    def save_packer_apk(
        self,
        host_sha256: str,
        guest_sha256: str,
        config_hash: str,
        config: Dict,
        content: IO,
        cmd: str,
    ) -> Sha256:
        pass

    @abstractmethod
    def save_screenshot(self, sha256: str, content: IO, **kwargs):
        pass

    @abstractmethod
    def pull_screenshot(self, sha256: str):
        pass

    @abstractmethod
    def get_all_metadata(self):
        pass

    @abstractmethod
    def update_metadata(self, sha256: str, **kwargs):
        pass

    @abstractmethod
    def get_metadata(self, sha256: str) -> Dict:
        pass

    @abstractclassmethod
    def from_env(cls):
        pass


class MongoStore(Store):
    def __init__(self, cache_dir=DEFAULT_MONGO_STORE_CACHE_DIR):
        LOGGER.warning("cache_dir isn't used for now...")
        self.cache_dir = (
            cache_dir if cache_dir is not None else DEFAULT_MONGO_STORE_CACHE_DIR
        )

    def iter(self) -> Generator[str, None, None]:
        for apk in Apk.objects:
            yield apk.sha256

    def pull(
        self,
        sha256: str,
        output_dir: Path = Path.cwd(),
        force: Optional[bool] = False,
    ):
        apk = Apk.objects.get(sha256=sha256)
        return apk.pull(output_dir, force=force)

    def save_apk(
        self,
        name: str,
        content: IO,
        screenshot: Optional[IO] = None,
        screenshot_metadata: Optional[Dict] = None,
    ) -> Sha256:
        apk = Apk(name=name, content=content)
        # handle the screenshots if any
        if screenshot is not None:
            apk.screenshot.put(screenshot)
        if screenshot_metadata is not None:
            apk.screenshot_meta = screenshot_metadata
        apk.save()
        return apk.sha256

    def save_packer_apk(
        self,
        host_sha256: str,
        guest_sha256: str,
        config_hash: str,
        config: Dict,
        content: IO,
        cmd: str,
    ) -> Sha256:
        host = Apk.objects.get(sha256=host_sha256)
        guest = Apk.objects.get(sha256=guest_sha256)
        papk = PackerApk(
            name="__packer_apk__",
            host=host,
            guest=guest,
            config_hash=config_hash,
            config=config,
            content=content,
            cmd=cmd,
        )
        papk.save()
        return papk.sha256

    def save_screenshot(self, sha256: str, content: IO, **kwargs):
        apk = Apk.objects.get(sha256=sha256)
        apk.screenshot.put(content)
        apk.screenshot_metadata = kwargs
        apk.save()

    def pull_screenshot(
        self,
        sha256: str,
        output_dir: Path = Path.cwd(),
        force: Optional[bool] = False,
    ) -> Path:
        apk = Apk.objects.get(sha256=sha256)
        return apk.pull_screenshot(output_dir, force=force)

    def get_all_metadata(self):
        """FIXME: need for similarity to be a 1st class citizen."""
        raise NotImplementedError

    def update_metadata(self, sha256: str, **kwargs):
        apk = Apk.objects.get(sha256=sha256)
        apk.metadata.update(kwargs)
        apk.save()

    def get_metadata(self, sha256: str):
        apk = Apk.objects.get(sha256=sha256)
        metadata = apk.metadata
        if isinstance(apk, PackerApk):
            metadata.update(
                host_sha256=apk.host.sha256,
                guest_sha256=apk.guest.sha256,
                name=apk.name,
                config=apk.config,
                cmd=apk.cmd,
            )
        return metadata

    @classmethod
    def from_env(cls):
        cache_dir = os.environ.get(ENV_MONGO_STORE_CACHE_DIR)
        return cls(cache_dir)


class FsStore(Store):
    def __init__(self, directory: Path = DEFAULT_FS_STORE_DIRECTORY):
        self.directory = (
            directory if directory is not None else DEFAULT_FS_STORE_DIRECTORY
        )
        self.directory.mkdir(parents=True, exist_ok=True)

    def _apk_dir(self, sha256):
        return self.directory / sha256

    def _apk_path(self, sha256):
        apk = self._apk_dir(sha256) / f"{sha256}.apk"
        metadata = apk.with_suffix(".apk.json")
        return apk, metadata

    def _screenshot_path(self, sha256):
        apk = self._apk_dir(sha256) / f"{sha256}.png"
        metadata = apk.with_suffix(".png.json")
        return apk, metadata

    def iter(self):
        for apk in self.directory.iterdir():
            # yield the directory name == sha256
            yield apk.name

    def pull(
        self, sha256: str, output_dir: Path = Path.cwd(), force: Optional[bool] = False
    ) -> Path:
        source, _ = self._apk_path(sha256)
        dest = output_dir / f"{sha256}.apk"
        return self._pull(source, dest, force)

    def save_apk(
        self,
        name: str,
        content: IO,
    ) -> Sha256:
        sha256 = to_sha256(content)
        p, m = self._apk_path(sha256)
        p.parent.mkdir(exist_ok=True, parents=True)
        if p.exists():
            raise ValueError(
                f"The file {p}(name={name}) already exists, not overwritting"
            )
        with p.open("wb") as f:
            f.write(content.read())
        # storing some metadata in a side-car (json) file
        # the name of the file will be <sha256>.apk.json (this is the metadata
        # of the apk file)
        self._update_metadata(m, name=name, sha256=sha256)
        return sha256

    def save_packer_apk(
        self, host_sha256, guest_sha256, config_hash, config, content, cmd
    ) -> Sha256:
        sha256 = self.save_apk("__packer_apk__", content)
        data = dict(
            sha256=sha256,
            host_sha256=host_sha256,
            guest_sha256=guest_sha256,
            cmd=cmd,
            config_hash=config_hash,
            config=config,
        )
        _, m = self._apk_path(sha256)
        self._update_metadata(m, **data)
        return sha256

    def save_screenshot(self, sha256: str, content: IO, **kwargs):
        self._apk_dir(sha256).mkdir(parents=True, exist_ok=True)
        p, m = self._screenshot_path(sha256)
        p.parent.mkdir(parents=True, exist_ok=True)
        with p.open("wb") as f:
            f.write(content.read())
        # storing some metadata in a side-car (json) file
        # the name of the file will be <sha256>.png.json (this is the metadata
        # of the png file)
        self._update_metadata(m, **kwargs)

    def pull_screenshot(
        self, sha256: str, output_dir: Path = Path.cwd(), force: Optional[bool] = False
    ):
        source, _ = self._screenshot_path(sha256)
        dest = output_dir / f"{sha256}.png"
        # TODO(msimonin): pull metadata
        return self._pull(source, dest, force)

    def get_all_metadata(self):
        """This might be heavy. So think about restrict its scope, maybe."""
        import joblib
        from joblib import Parallel, delayed

        def get_metadata(apks: Iterable[Path]) -> Iterable[Path]:
            result = []
            for idx, apk in enumerate(apks):
                # metadata are stored in json
                _metadata = {}
                for metadata in apk.glob("*.json"):
                    if metadata.exists():
                        with metadata.open("r") as f:
                            _metadata.update(json.load(f))
                # include the sha256
                _metadata.update(sha256=apk.name)
                result.append(_metadata)
                # debug
                if idx % 1000 == 0:
                    print(idx)
            return result

        n = joblib.cpu_count()
        LOGGER.debug(f"count_screenshots using {n} cores")
        slices = [slice(i, -1, n) for i in range(n)]

        # map
        apks = list(self.directory.iterdir())
        apks_slices = [apks[s] for s in slices]
        joblib_apks = Parallel(n_jobs=n)(
            delayed(get_metadata)(_apks) for _apks in apks_slices
        )
        # reduce
        result = []
        for joblib_apk in joblib_apks:
            result.extend(joblib_apk)
        return result

    def update_metadata(self, sha256: str, **kwargs):
        _, m = self._apk_path(sha256)
        self._update_metadata(m, **kwargs)

    def get_metadata(self, sha256: str) -> Dict:
        _, m = self._apk_path(sha256)
        metadata = {}
        with m.open("r") as f:
            metadata = json.load(f)
        return metadata

    def _update_metadata(self, metadata_path: Path, **kwargs):
        metadata_path.parent.mkdir(parents=True, exist_ok=True)
        metadata = {}
        if metadata_path.exists() and metadata_path.is_file():
            with metadata_path.open("r") as f:
                metadata = json.load(f)
        metadata.update(**kwargs)
        # dump it back
        with metadata_path.open("w") as f:
            json.dump(metadata, f)

    def _pull(self, source: Path, dest: Path, force):
        # just copying the file
        if dest.exists() and dest.is_file() and not force:
            return dest
        dest.write_bytes(source.read_bytes())
        return dest

    @classmethod
    def from_env(cls):
        directory = os.environ.get(ENV_FS_STORE_DIRECTORY, DEFAULT_FS_STORE_DIRECTORY)
        return cls(directory=Path(directory))


def get_store():
    """Gets the reference to the API cient (singleton)."""
    with _store_lock:
        global _store
        if _store is not None:
            return _store

        # build the store from the env
        store_key = os.environ.get(ENV_PACKER_STORE)
        if store_key == "mongo":
            connection = os.environ.get(ENV_MONGO_STORE_CONNECTION)
            if connection is not None:
                from mongoengine import connect

                LOGGER.info(f"Connecting to {connection}")
                connect(host=connection)
            _store = MongoStore.from_env()
        else:
            _store = FsStore.from_env()

        return _store


def populate_with_app(
    app_path: Path, store: Optional[Store] = None, skip_error: bool = False
):
    if store is None:
        store = get_store()
    sha256 = None
    with app_path.open("rb") as f:
        LOGGER.info(f"Uploading {app_path}")
        try:
            sha256 = store.save_apk(app_path.name, f)
            LOGGER.info(f"[sha256] {sha256}")
        except Exception as e:
            LOGGER.error(f"There's an error with {app_path} [skip_error={skip_error}]")
            if not skip_error:
                raise e
        return sha256


def populate_with_apps(
    directory: Path,
    recursive: bool,
    store: Optional[Store] = None,
    skip_error: bool = False,
):
    if store is None:
        store = get_store()
    if recursive:
        glob = "**/*.apk"
    else:
        glob = "*.apk"
    for app_path in directory.glob(glob):
        populate_with_app(app_path, skip_error=skip_error)


def main():
    logging.basicConfig(level=logging.DEBUG)
    parser = argparse.ArgumentParser(
        description="Upload the content of an apk (directory) to the store"
    )
    parser.add_argument(
        "path",
        type=str,
        help="If path is a directory, every *.apk will be considered in this directory",
    )
    parser.add_argument(
        "-r",
        "--recursive",
        action="store_true",
        help="If the path is a directory do a recursive look up of apk",
    )
    parser.add_argument("--skip", action="store_true", help="Skip any error")
    args = parser.parse_args()
    print(args)
    path = Path(args.path)
    if path.is_dir():
        populate_with_apps(path, args.recursive, skip_error=args.skip)
    else:
        populate_with_app(path, skip_error=args.skip)

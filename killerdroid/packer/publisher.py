import argparse
from itertools import product
import json
import logging
import os
from pathlib import Path
from typing import Iterable, Optional, Dict, Mapping

from joblib.parallel import Parallel, delayed

from .conf import all_confs


LOGGER = logging.getLogger(__name__)

MANDATORY_KEYS = [
    "host_sha256",
    "guest_sha256",
    "packer_config",
    "config",
    "config_hash",
]

SWEEP_DIR = "sweeps"

# maximum
ACK_DEADLINE_SECONDS = 600

# This config is intended to be removed eventually
DEFAULT_PACKER_CONFIG = {
    "outputDirectory": "output",
    "requiredFiles": "./required_files",
    "pathToKeystore": "",
    # remove this later when packer won't need a database backend anymore
    "databaseConfiguration": {"connectToDatabase": False},
}

# generate all conf > mongo
# generate all conf > fichiers > sweeper/pulsar/gcp
# compare conf in mongo et genere les manquantes > sweeper/pulsar/gcp

"""
def mongo_dump_conf():
    for hashcode, conf in all_confs():
        c = Config(name=hashcode, content=conf)
        c.save()


def fs_dump_conf(output_dir: Path):
    import json

    output_dir.mkdir(parents=True, exist_ok=True)
    for hashcode, conf in all_confs():
        conf_file = output_dir / f"{hashcode}.json"
        with conf_file.open("w") as f:
            json.dump(conf, f)
"""


def generate_confs(benign_sha: str, malicious_sha: str) -> Iterable:
    """This is basically what we'll send to our queue."""
    tactics = all_confs()
    results = []
    for hashcode, tactic in tactics:
        # keep legacy keys
        results.append(
            dict(
                packer_config=DEFAULT_PACKER_CONFIG,
                config=tactic,
                config_hash=hashcode,
                host_sha256=benign_sha,
                guest_sha256=malicious_sha,
            )
        )
    return results


def _all_confs(
    benign_shas: Iterable[str],
    malicious_shas: Iterable[str],
    limit: Optional[int] = None,
    random: bool = False,
) -> Iterable:
    # all confs
    if limit is None:
        limit = -1
    all_confs = []
    confs = Parallel(n_jobs=-1)(
        delayed(generate_confs)(b, m) for b, m in product(benign_shas, malicious_shas)
    )
    for c in confs:
        all_confs.extend(c)

    if random:
        from random import shuffle

        shuffle(all_confs)
    all_confs = all_confs[0:limit]
    return all_confs


def publish_sweeps(all_confs, provenance: str = ""):
    LOGGER.info("publishing to a sweep")
    from execo_engine import ParamSweeper, HashableDict

    def to_hashable_dict(d) -> HashableDict:
        h = HashableDict()
        if isinstance(d, list):
            return [to_hashable_dict(v) for v in d]
        for k, v in d.items():
            if isinstance(v, dict):
                h[k] = to_hashable_dict(v)
            else:
                h[k] = v
        return h

    sweeps = [to_hashable_dict(c) for c in all_confs]
    LOGGER.info(f"Generated {len(sweeps)} confs")
    sweep_dir = SWEEP_DIR
    if provenance != "":
        sweep_dir = f"{SWEEP_DIR}_{provenance}"
    ParamSweeper(sweep_dir, sweeps=sweeps, save_sweeps=True)


def publish_gcp(all_confs: Iterable[Dict], provenance: str = ""):
    LOGGER.info("publishing to gcp")
    from google.cloud import pubsub_v1
    from google.api_core.exceptions import AlreadyExists

    publisher = pubsub_v1.PublisherClient()
    # align with minionize
    project_id = os.getenv("GOOGLE_PROJECT_ID")
    if project_id is None:
        raise ValueError("GOOGLE_PROJECT_ID is missing in the env")
    topic_id = os.getenv("GOOGLE_TOPIC_ID")
    if topic_id is None:
        raise ValueError("GOOGLE_TOPIC_ID is missing in the env")
    subscription = os.getenv("GOOGLE_SUBSCRIPTION")
    if subscription is None:
        raise ValueError("GOOGLE_SUBSCRIPTION is missing in the env")

    # create the subscription
    # we create it here to make sure the client on the same subscription will get past messages
    subscriber = pubsub_v1.SubscriberClient()
    topic_path = f"projects/{project_id}/topics/{topic_id}"
    # now let's publish
    publisher = pubsub_v1.PublisherClient()
    try:
        publisher.create_topic(request={"name": topic_path})
    except AlreadyExists as e:
        LOGGER.warn(
            f"Topic {topic_path} already exists, but that's not a problem in general"
        )

    subscription_path = subscriber.subscription_path(project_id, subscription)
    try:
        subscriber.create_subscription(
            request={
                "name": subscription_path,
                "topic": topic_path,
                "ack_deadline_seconds": ACK_DEADLINE_SECONDS,
            }
        )
    except AlreadyExists as e:
        LOGGER.warn(
            f"Subscription {subscription} already exists, but thats not a problem in general"
        )

    for message in all_confs:
        publisher.publish(topic_path, json.dumps(message).encode("utf-8"))


def _publish_confs(engine: str, all_confs: Mapping):
    def drain(conf):
        """keep only the mandatory keys."""
        c = {}
        for key in MANDATORY_KEYS:
            c[key] = conf[key]
        return c

    _all_confs = [drain(c) for c in all_confs]
    all_confs_file = Path("all_confs.json")
    LOGGER.info(f"Writing the {len(_all_confs)} confs to {all_confs_file}")
    # dump the confs as a json for repeatability
    with Path("all_confs.json").open("w") as f:
        f.write(json.dumps(all_confs))

    if engine == "gcp":
        publish_gcp(_all_confs, "packer")
    elif engine == "sweep":
        publish_sweeps(_all_confs, "packer")
    elif engine == "stdout":
        print(json.dumps(_all_confs))
    else:
        raise ValueError(f"{engine} is an invalid engine.")


def _packer_gen(args):
    all_confs = _all_confs(
        args.benign_shas, args.malicious_shas, limit=args.limit, random=args.random
    )
    _publish_confs(args.engine, all_confs)


def _packer_json(args):
    # TODO validate schema (write it first)
    all_confs = []
    with Path(args.file).open("r") as f:
        for l in f.readlines():
            d = json.loads(l)
            d["packer_config"] = DEFAULT_PACKER_CONFIG
            all_confs.append(d)
    _publish_confs(args.engine, all_confs)


def _similarity(args):
    from .io import get_store

    store = get_store()
    # beware when dealing with 100s of thousand of apks
    all_confs = [{"sha256": sha256} for sha256 in store.iter()]
    if args.engine == "gcp":
        publish_gcp(all_confs, "similarity")
    elif args.engine == "sweep":
        publish_sweeps(all_confs, "similarity")
    elif args.engine == "stdout":
        print(all_confs)
    else:
        raise ValueError(f"{args.engine} is an invalid engine.")


def main():
    logging.basicConfig(level=logging.DEBUG)
    parser = argparse.ArgumentParser(
        description="Publish the configs to various sinks (gcp, sweeps)"
    )
    # FIXME
    parser.set_defaults(func=lambda x: x)

    subparsers = parser.add_subparsers(help="subcommand to create specific queue")
    # Packer
    parser_packer = subparsers.add_parser("packer", help="Generate packer inputs")
    # gen subcommand

    subsubparsers = parser_packer.add_subparsers(help="method to generate the inputs")
    parser_packer_gen = subsubparsers.add_parser(
        "gen", help="Generate all the possible inputs from the given parameter"
    )

    # kd-publish packer gen -b ... -m ... --limit ... -e sweep
    parser_packer_gen.add_argument(
        "--limit", "-l", type=int, help="limit the number of conf"
    )
    parser_packer_gen.add_argument(
        "-b", "--benign_shas", type=str, nargs="+", help="sha256"
    )
    parser_packer_gen.add_argument(
        "-m", "--malicious_shas", type=str, nargs="+", help="sha256"
    )
    parser_packer_gen.add_argument(
        "--random", action="store_true", help="randomize the confs"
    )
    parser_packer_gen.add_argument(
        "-e", "--engine", type=str, help="publisher (sweep,gcp, stdout)", required=True
    )
    parser_packer_gen.set_defaults(func=_packer_gen)

    # kd-publish packer json <file> -e ...
    parser_packer_json = subsubparsers.add_parser(
        "json", help="Generate the inputs corresponding to the json line files"
    )
    parser_packer_json.add_argument(
        "file", type=str, help="path to the json lines file to read"
    )
    parser_packer_json.add_argument(
        "-e", "--engine", type=str, help="publisher (sweep,gcp, stdout)", required=True
    )
    parser_packer_json.set_defaults(func=_packer_json)

    # screenshots
    parser_sim = subparsers.add_parser(
        "similarity",
        help="Generate the similarity inputs for all apk in a store (must be set)",
    )
    parser_sim.add_argument(
        "-e", "--engine", type=str, help="publisher (sweep,gcp,stdout)"
    )
    parser_sim.set_defaults(func=_similarity)

    args = parser.parse_args()
    args.func(args)

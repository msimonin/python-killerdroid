import logging
from pathlib import Path
from typing import IO

from mongoengine import *  # noqa: F405
from mongoengine.fields import *  # noqa: F405

LOGGER = logging.getLogger(__name__)


def to_sha256(b: IO):
    """Compute the sha256 for the file with path path.

    This is taken from:
    https://gitlab.inria.fr/lgitzing/killerdroid_core/-/blob/11ec24a4890785b87fbb646585f165c993559726/killerdroid_core/utils.py#L95-104
    """
    import hashlib

    sha256_hashcode = hashlib.sha256()
    for byte_block in iter(lambda: b.read(4096), b""):
        sha256_hashcode.update(byte_block)
    # reset stream
    b.seek(0)
    return sha256_hashcode.hexdigest().lower()


class Apk(DynamicDocument):
    # TODO(msimonin):
    # cache sha256 somewhere in an attribute
    name = StringField(required=True)
    content = FileField(required=True)
    metadata = DictField()

    # computed field
    sha256 = StringField(unique=True)

    # store a screenshot of the app
    # alongside some meta data
    screenshot = ImageField()
    # should store at least the emulator version
    screenshot_meta = DictField()

    meta = {"allow_inheritance": True, "indexes": ["sha256"]}

    def __repr__(self):
        return f"<Apk name={self.name} sha256={self.sha256}>"

    def clean(self):
        """Some prevalidation."""
        self.sha256 = to_sha256(self.content)

    def _target_path(self, output_dir: Path):
        # sha256 must exists !
        # NOTE(msimonin): seems that the suffix is important for packer...
        # see https://gitlab.inria.fr/lgitzing/killerdroid_packer/-/issues/5
        target_path = output_dir / Path(f"{self.sha256}.apk")
        return target_path

    def _screenshot_path(self, output_dir: Path):
        # NOTE(msimonin): We assume only png files ...
        screenshot_path = output_dir / f"{self.sha256}.png"
        return screenshot_path

    def _target_exists(self, output_dir: Path):
        # sha256 must exists !
        if (
            self._target_path(output_dir).exists()
            and self._target_path(output_dir).is_file()
        ):
            local_sha256 = to_sha256(open(self._target_path(output_dir), "rb"))
            if local_sha256 == self.sha256:
                return True
        return False

    def pull(self, output_dir: Path, force: bool = False) -> Path:
        output_dir.mkdir(parents=True, exist_ok=True)
        target_path = self._target_path(output_dir)
        if self._target_exists(output_dir) and not force:
            LOGGER.info(
                f"target app already downloaded in {output_dir}, use force to download"
            )
            return target_path
        LOGGER.info(f"Downloading APK to {self._target_path(output_dir)}")
        with self._target_path(output_dir).open("wb") as f:
            f.write(self.content.read())

        return target_path

    def pull_screenshot(self, output_dir: Path, force: bool = False) -> Path:
        output_dir.mkdir(parents=True, exist_ok=True)
        screenshot_path = self._screenshot_path(output_dir)
        if screenshot_path.exists() and not force:
            LOGGER.info(
                f"screenshot already downloaded in {output_dir}, use force to download"
            )
            return screenshot_path
        LOGGER.info(f"Downloading screenshot to {self._screenshot_path(output_dir)}")
        with self._screenshot_path(output_dir).open("wb") as f:
            f.write(self.screenshot.read())

        return screenshot_path


class PackerApk(Apk):
    host = ReferenceField(Apk, required=True)
    guest = ReferenceField(Apk, required=True)
    # TODO(msimonin): validate this
    config_hash = StringField(required=True)
    config = DictField(required=True)

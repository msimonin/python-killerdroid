from pathlib import Path
import logging
import os
import subprocess
from subprocess import TimeoutExpired, check_output
import tempfile
import time
from typing import List, Optional
from uuid import uuid4

from faker import Faker

LOGGER = logging.getLogger(__name__)

DIRECTORY = Path(__file__).parent.resolve()
PATH_TO_AUTOGEN = DIRECTORY / "autogenkey.sh"

ANDROID_HOME = Path(os.getenv("ANDROID_HOME"))
if ANDROID_HOME is None:
    raise ValueError("ANDROID_HOME not set")

ADB_PATH = ANDROID_HOME / "platform-tools" / "adb"
# shortcut
ADB = str(ADB_PATH)

START_ACTIVITY_TIMEOUT = 5


def shell(command: List[str], **kwargs):
    """Util function that wraps subprocess.run"""
    LOGGER.debug(f"Launching {' '.join(command)} with {kwargs}")
    return subprocess.run(command, **kwargs)


def find_build_tools(must_include: Optional[List[str]] = None) -> Optional[Path]:
    """Find suitable build tools.

    This is taken from
    https://gitlab.inria.fr/lgitzing/killerdroid_core/-/blob/11ec24a4890785b87fbb646585f165c993559726/killerdroid_core/utils.py

    ANDROID_HOME must be set
    """
    build_tools = ANDROID_HOME / "build-tools"
    if must_include is None:
        must_include = None

    if not build_tools.exists():
        raise FileNotFoundError(f"build-tools not found in {ANDROID_HOME}")
    # Select build tools version
    prefered_versions = [
        "29.0.3",
        "28.0.3",
        "27.0.3",
        "28.0.0",
        "28.0.1",
        "28.0.2",
    ]

    def include_all(bt: Path, must_include: List[str]):
        """Check if the must include bin are all in the build_tool path."""
        if not must_include:
            return True
        return all([bt.joinpath(b).exists() for b in must_include])

    build_tools_path = None
    for version in prefered_versions:
        build_tools_path = build_tools / version
        if build_tools_path.exists() and include_all(build_tools_path, must_include):
            return build_tools_path.resolve()

    # if not found let's find in what we have
    # by scanning the directory
    for v_dir in build_tools.iterdir():
        build_tools_path = build_tools / v_dir
        if os.path.exists(build_tools_path) and include_all(
            build_tools_path, must_include
        ):
            return build_tools_path
    raise FileNotFoundError("build-tools not found")


def find_tool(tool: str) -> Path:
    """Find a tool in the build tool jungle.

    ANDROID_HOME must be set

    Args:
        tool: the tool name to find (e.g aapt2)

    Returns:
        The path to the tool if it is found

    Raises:
        FileNotFoundError if the tool isn't found
    """
    build_tools_path = find_build_tools([tool])
    path = build_tools_path / tool
    if not path.exists():
        raise FileNotFoundError(f"aapt2 bin not found at {path}")
    return path


def package_name(apk: Path) -> str:
    """Get the package name

    This is based on aapt2 tool and therefore must be found on your system.
    ANDROID_HOME must be set

    Args:
        apk: path to the package

    Returns:
        The package name

    Raises:
        FileNotFoundError if aapt2 can't be found
    """
    aapt2_path = find_tool("aapt2")

    command = [aapt2_path, "dump", "packagename", str(apk)]

    completed_process = subprocess.run(command, capture_output=True, check=True)
    for line in completed_process.stdout.decode("utf-8").split("\n"):
        return line


def launchable_activities(apk: Path) -> List[str]:
    """Find all the launchable activities of a package based on aapt2.

    This is talen from
    https://gitlab.inria.fr/lgitzing/av-auditor/-/blob/fc1da4bd7432f574229e311e31edaeddef743e9a/killerdroid/utils.py#L402

    ANDROID_HOME must be set

    Args:
        apk: path to the apk to inspect

    Returns:
        The list of the activity to launch (e.g com.lock.app.StartShowActivity)

    Raises:
        FileNotFoundError if aapt2 can't be found
    """
    aapt2_path = find_tool("aapt2")

    command = [aapt2_path, "dump", "badging", str(apk)]
    completed_process = subprocess.run(command, capture_output=True, check=True)

    launchable_activities = []
    for line in completed_process.stdout.decode("utf-8").split("\n"):
        if "launchable-activity" in line:
            for s in line.split(" "):
                if "name=" in s:
                    launchable_activities.append(
                        s.replace("name=", "").replace("'", "")
                    )
    return launchable_activities


def _sign_apk(apk_path: Path, output_path: Path, key: bytes, cert: bytes):
    """Sign an apk given a key and cert based on apksigner.

    Taken from:
    https://gitlab.inria.fr/lgitzing/killerdroid_core/-/blob/11ec24a4890785b87fbb646585f165c993559726/killerdroid_core/Apk.py#L180

    This assumes #1 signing method (private key and certificate)
    The key must be PKCS8 and DER (so as the cert), this is an Android
    requirement.

    Args:
        apk_path: path to the apk to sign
        output_path: path to the signed apk
        key: PKCS8 and DER private key
        cert: DER x509 certificate
    """
    LOGGER.info(f"Signin apk at path: {os.path.abspath(apk_path)}")
    LOGGER.info(f"Wanted signed full path: {os.path.abspath(output_path)}")
    bin_name = "apksigner"
    build_tools = find_build_tools(must_include=[bin_name])
    apksigner_path = build_tools / bin_name
    if not apksigner_path:
        raise FileNotFoundError("No apksigner executable found.")
    if not apk_path.exists():
        raise FileNotFoundError(f"Original apk given not found: {apk_path}")
    output_path.parent.mkdir(parents=True, exist_ok=True)

    command = [
        str(apksigner_path),
        "sign",
        "-v",
        "--min-sdk-version",
        "20",
        "--key",
        str(key),
        "--cert",
        str(cert),
        "--out",
        str(output_path),
        "--in",
        str(apk_path),
    ]
    LOGGER.debug(" ".join(command))
    completed_process = subprocess.run(command, capture_output=True)
    for line in completed_process.stdout.decode("utf-8").split("\n"):
        if "Signed" in line and os.path.exists(output_path):
            LOGGER.info(
                f"Successfully signed apk. Output path: {os.path.abspath(output_path)}"
            )
            return True
    for line in completed_process.stderr.decode("utf-8").split("\n"):
        LOGGER.error(line)
    LOGGER.error(f"Could not sign apk at path: {os.path.abspath(apk_path)}")
    return False


def generate_selfsigned_cert():
    """Generates self signed certificate for a hostname, and optional IP addresses.

    From https://gist.github.com/bloodearnest/9017111a313777b9cce5

    Note:
        apksigner requires PKCS8(serialisation) + DER(encoding)

    Returns:
        private key and cert as bytes
    """
    from cryptography import x509
    from cryptography.x509.oid import NameOID
    from cryptography.hazmat.primitives import hashes
    from cryptography.hazmat.backends import default_backend
    from cryptography.hazmat.primitives import serialization
    from cryptography.hazmat.primitives.asymmetric import rsa
    from datetime import datetime, timedelta

    # random inputs
    faker = Faker("en_US")
    hostname = faker.first_name().lower()

    # Generate our key
    key = rsa.generate_private_key(
        public_exponent=65537,
        key_size=2048,
        backend=default_backend(),
    )

    name = x509.Name(
        [
            x509.NameAttribute(NameOID.COMMON_NAME, hostname),
            x509.NameAttribute(NameOID.COUNTRY_NAME, faker.country()[0:2]),
            x509.NameAttribute(NameOID.STATE_OR_PROVINCE_NAME, faker.state()),
            x509.NameAttribute(NameOID.LOCALITY_NAME, faker.city()),
            x509.NameAttribute(NameOID.ORGANIZATION_NAME, faker.company()),
        ]
    )

    # best practice seem to be to include the hostname in the SAN, which *SHOULD* mean COMMON_NAME is ignored.
    # alt_names = [x509.DNSName(hostname)]

    # san = x509.SubjectAlternativeName(alt_names)

    # path_len=0 means this cert can only sign itself, not other certs.
    basic_contraints = x509.BasicConstraints(ca=True, path_length=0)
    now = datetime.utcnow()

    cert = (
        x509.CertificateBuilder()
        .subject_name(name)
        .issuer_name(name)
        .public_key(key.public_key())
        .serial_number(1000)
        .not_valid_before(now)
        .not_valid_after(now + timedelta(days=10 * 365))
        .add_extension(basic_contraints, False)
        # .add_extension(san, False)
        .sign(key, hashes.SHA256(), default_backend())
    )
    cert_pem = cert.public_bytes(encoding=serialization.Encoding.DER)
    key_pem = key.private_bytes(
        encoding=serialization.Encoding.DER,
        format=serialization.PrivateFormat.PKCS8,
        encryption_algorithm=serialization.NoEncryption(),
    )

    return cert_pem, key_pem


def sign(apk: Path, working_dir=Path.cwd()):
    """Sign an apk.

    This function generates on-th-fly signature of the app.

    Args:
        apk        : Path
        working_dir: the directory will store the generated files (e.g key,
                     cert, signed apk)
    """
    # TODO(msimonin): handle resigning
    LOGGER.info(f"Start signin process for apk: {apk}")
    _cert, _key = generate_selfsigned_cert()
    # writing the files
    key_path = working_dir / "key"
    with open(key_path, "wb") as f:
        f.write(_key)
    cert_path = working_dir / "cert"
    with open(cert_path, "wb") as f:
        f.write(_cert)
    signed_path = working_dir / "signed.apk"
    LOGGER.debug(f"SIGNED PATH: {signed_path}")
    output = _sign_apk(apk, signed_path, key_path, cert_path)
    if not output:
        RuntimeError("Cannot sign apk")
    LOGGER.info("Signin process done.")
    return signed_path


def wait_for_device():
    """Wait for the emulator to be ready.

    Raises:
        TimeoutExpired if the timeout is reached (180s for now)
    """
    command = [
        ADB,
        "wait-for-device",
        "shell",
        "while [[ -z $(getprop sys.boot_completed) ]]; do sleep 1; done; input keyevent 82",
    ]
    LOGGER.info("Waiting for the device to be ready...")
    cp = subprocess.run(command, capture_output=True, check=True, timeout=180)
    for line in cp.stdout.decode("utf-8").split("\n"):
        LOGGER.debug(line)
    for line in cp.stderr.decode("utf-8").split("\n"):
        LOGGER.debug(line)

    LOGGER.info("A device is ready, proceeding.")
    return True


def getprop() -> str:
    """Get the property of *THE* associated device."""
    command = [ADB, "shell", "getprop"]
    LOGGER.info("Getting the property of the device")
    cp = subprocess.run(command, capture_output=True, check=True, timeout=180)
    return cp.stdout.decode("utf-8")


def start_activity(package_name: str, activity_name: str):
    """Start an activity
    From
    https://gitlab.inria.fr/lgitzing/av-auditor/-/blob/fc1da4bd7432f574229e311e31edaeddef743e9a/killerdroid/screenshot/main.py#L22

    FIXME 10/10/20: we don't wait anymore so the return value is not relevant anymore
    """
    _a = activity_name.replace("$", "\\$")
    str_start = f"{package_name}/{_a}"
    # FIXME(msimonin): original command was using -W == Wait completion
    # but seems to block infinetly for some app...(koler...)
    # This might be due to some apk (malware) blocking the main thread
    # To circumvent this situation we add a timeout to this command.
    command = [
        ADB,
        "shell",
        "am",
        "start",
        "-n",
        str_start,
        # We also generate an intent supposed to be generated when
        # clicking on the launcher
        # https://developer.android.com/guide/components/intents-filters#Types
        "-a",
        "android.intent.action.MAIN",
        "-c",
        "android.intent.category.LAUNCHER",
    ]
    LOGGER.info(command)
    try:
        completed_process = shell(
            command, capture_output=True, timeout=START_ACTIVITY_TIMEOUT
        )
        for line in completed_process.stdout.decode("utf-8").split("\n"):
            if "Complete" in line:
                return True
    except TimeoutExpired as e:
        LOGGER.warn(e)
        LOGGER.warn(
            "Can't ensure the activity is launched, proceeding anyway but result might be biased"
        )
    return False


def install_package(apk: Path):
    """Install an apk to *THE* adb reachable device.

    This is taken from https://gitlab.inria.fr/lgitzing/av-auditor/-/blob/fc1da4bd7432f574229e311e31edaeddef743e9a/killerdroid/utils.py#L170
    (error management more idiomatic)
    """
    LOGGER.info(f"installing apk: {apk}")
    completed_process = subprocess.run(
        [ADB, "install", "-r", str(apk)], capture_output=True, timeout=20, check=True
    )
    stdout_lines = completed_process.stdout.decode("utf-8")
    stderr_lines = completed_process.stderr.decode("utf-8")
    if "Success" in stdout_lines:
        # check the package list
        pn = package_name(apk)
        installed = pn in list_packages()
        if installed:
            LOGGER.info(f"Successfully installed apk: {apk}")
            return
        else:
            raise Exception(f"{pn} not in the installed list")

    LOGGER.error(f"Failed to install apk. Error: {stderr_lines}")
    raise Exception(stderr_lines)


def uninstall_third_party_packages(exclude=None):
    """
    This function uninstall all third party (user?) apps on the device.
    Apps can be prevented from being uninstalled by putting their package name
    in the exclude list argument
    """
    LOGGER.info("Uninstalling third party packages...")
    if exclude is None:
        exclude = []
    third_party = list_third_party_packages()
    for pname in third_party:
        if pname and not pname in exclude:
            LOGGER.info(f"Uninstalling app: {pname}")
            command = [ADB, "shell", "pm", "uninstall", pname]
            completed_process = subprocess.run(command, capture_output=True)
            if "Success" in completed_process.stdout.decode("utf-8"):
                LOGGER.info(f"Successfully uninstalled app {pname}")


def _list_packages(extra_args: str = "") -> List[str]:
    """List all installed packages in *THE* adb reachable device."""
    cmd = [ADB, "shell", "cmd", "package", "list", "packages"]
    if extra_args != "":
        cmd.append(extra_args)
    completed_process = subprocess.run(
        cmd,
        capture_output=True,
        timeout=20,
        check=True,
    )
    stdout_lines = completed_process.stdout.decode("utf-8")
    # package:com.android.providers.userdictionary
    # package:com.android.emergency
    # [...]
    # package:com.android.location.fused
    return [l.replace("package:", "") for l in stdout_lines.strip().split("\n")]


def list_packages() -> List[str]:
    return _list_packages()


def list_third_party_packages() -> List[str]:
    return _list_packages("-3")


# TODO(msimonin) return IO ?
def screenshot(screenshot_path: Path, name_on_device: str = "screenshot.png") -> Path:
    """Take a screenshot of *THE* adb reachable device.

    Beware of the space it will take if subsequent screenshots are named differently.
    """
    screenshot_path.parent.mkdir(parents=True, exist_ok=True)
    # Note the image can be streamed directly on stdout.
    command = [ADB, "shell", "screencap", f"/sdcard/{name_on_device}"]
    command2 = [ADB, "pull", f"/sdcard/{name_on_device}", str(screenshot_path)]
    subprocess.check_call(command)
    subprocess.check_call(command2)
    return screenshot_path

import argparse
import logging
from pathlib import Path
import tempfile
from typing import Iterable
import time

from killerdroid.packer.io import get_store
from killerdroid.utils import (
    getprop,
    install_package,
    uninstall_third_party_packages,
    launchable_activities,
    package_name,
    screenshot,
    sign,
    start_activity,
    wait_for_device,
)

import cv2
from skimage.metrics import structural_similarity

LOGGER = logging.getLogger(__name__)


def _compare(reference_screenshot: Path, screenshot: Path):
    image_a = cv2.imread(str(reference_screenshot))
    image_b = cv2.imread(str(screenshot))
    grayA = cv2.cvtColor(image_a, cv2.COLOR_BGR2GRAY)
    grayB = cv2.cvtColor(image_b, cv2.COLOR_BGR2GRAY)
    similarity = structural_similarity(grayA, grayB)
    return similarity


def _take(apks: Iterable[str] = None):
    store = get_store()

    wait_for_device()
    metadata = dict(getprop=getprop())

    for sha256 in apks:
        print(f"---> {sha256}")
        uninstall_third_party_packages()
        # api smell we should get an apk object here that wraps the store items
        # with an IO attribute for the apk
        with tempfile.TemporaryDirectory() as tmp:
            apk_path = store.pull(sha256, output_dir=Path(tmp)).resolve()
            signed_apk_path = sign(apk_path, Path(tmp))
            activities = launchable_activities(signed_apk_path)
            name = package_name(signed_apk_path)
            # FIXME(msimonin): one apk could have several activities but for
            # now let's consider only one. This means that we suppose that the
            # reference image (=malware activity is trigeered on the first
            # activity).
            assert len(activities) == 1
            a = activities[0]
            install_package(signed_apk_path.resolve())
            start_activity(name, a)
            # wait a bit ...
            with tempfile.TemporaryDirectory() as tmp:
                similarity = 0
                previous = Path(tmp) / "a.png"
                current = Path(tmp) / "b.png"
                # take initial screenshot
                screenshot(previous)
                while similarity < 0.99:
                    time.sleep(5)
                    screenshot(current)
                    similarity = _compare(previous, current)
                    previous.write_bytes(current.read_bytes())
                    LOGGER.info(f"similarity = {similarity}")
                # image has stabilized save it
                screenshot_path = Path(tmp) / f"{sha256}.png"
                s = screenshot(screenshot_path)
                store.save_screenshot(sha256, s.open("rb"), **metadata)
            # write the metadata


def _validate(sha256s: Iterable[str], **kwargs):
    store = get_store()
    for sha256 in sha256s:
        metadata = store.get_metadata(sha256)
        guest_sha256 = metadata.get("guest_sha256")
        if guest_sha256 is None:
            LOGGER.warn(f"No guest_sha256 defined for {sha256}...skipping")
        else:
            screenshot_similarity(sha256, guest_sha256, **kwargs)


def screenshot_similarity(
    sha256: str,
    reference_screenshot_sha256: str,
    timeout: int = 30,
    threshold=0.95,
):
    """Install an app and compare the screen with the reference screenshot.

    This uses a strucutural similarity function.
    see: https://scikit-image.org/docs/dev/auto_examples/transform/plot_ssim.html
    """

    store = get_store()

    wait_for_device()
    metadata = dict(getprop=getprop())
    print(metadata)
    # api smell we should get an apk object here that wraps the store items
    # with an IO attribute for the apk
    with tempfile.TemporaryDirectory() as tmp:
        apk = store.pull(sha256, output_dir=Path(tmp))
        signed_apk_path = sign(apk, Path(tmp))
        activities = launchable_activities(signed_apk_path)
        name = package_name(signed_apk_path)
        reference_screenshot = store.pull_screenshot(
            reference_screenshot_sha256, Path(tmp)
        )
        # we try to find an activity that triggers the guest apk (e.g malicious
        # app) we clean the env before testing the next activity to start with
        # "fresher" env ideally we'd need to restart the whole emulator (think
        # about malicious apps that encrypts the disk)
        for a in activities:
            uninstall_third_party_packages()
            install_package(signed_apk_path)
            start_activity(name, a)
            screenshot_path = Path(tmp) / "screenshot.png"
            start = time.time()  # s (float)
            elapsed = time.time() - start
            similarity = 0
            s = None
            while elapsed < timeout and similarity < threshold:
                # api smell
                s = screenshot(screenshot_path)
                similarity = _compare(reference_screenshot, s)
                LOGGER.info(f"[{a}] screenshot_similarity = {similarity}")
                elapsed = time.time() - start
                time.sleep(1)
            if s is not None:
                # compress ?
                # FIXME(msimonin): we overwrite in case of multiple activities ...
                store.save_screenshot(
                    sha256,
                    content=s.open("rb"),
                    structural_similarity=similarity,
                    activity=a,
                    **metadata,
                )
            if similarity >= threshold:
                # leave once we got one activity that matches the criteria
                break


def cli_take(args):
    store = get_store()
    sha256s = args.sha256s
    if sha256s is None or sha256s == []:
        sha256s = store.iter()
    _take(sha256s)


def cli_validate(args):
    store = get_store()
    sha256s = args.sha256s
    if sha256s is None or sha256s == []:
        sha256s = store.iter()
    _validate(sha256s)


def main():
    logging.basicConfig(level=logging.DEBUG)
    parser = argparse.ArgumentParser(
        description="""
Install apps on android (adb) from a store and take a screenshot after some time.

This requires a (single) emulator reachable using adb. All apks of
the store will be installed. This function is well suited for local
testing with a small number of applications.

Environment:
    Store environment variables (STORE, FS_STORE_DIRECTORY, MONGO_STORE_CONNECTION...)
""",
        formatter_class=argparse.RawTextHelpFormatter,
    )
    # FIXME
    parser.set_defaults(func=lambda x: x)

    subparsers = parser.add_subparsers(help="plop")
    parser_take = subparsers.add_parser(
        "take", help="Take screenshot(s) of an application"
    )
    parser_take.add_argument(
        "sha256s",
        nargs="*",
        help="Install and take screenshot for this apk",
    )
    parser_take.set_defaults(func=cli_take)

    parser_validate = subparsers.add_parser(
        "validate", help="Validate an application (using structural similarity)"
    )
    parser_validate.add_argument(
        "sha256s",
        nargs="*",
        help="Install and validate this application (sha256 identified) from the store",
    )
    parser_validate.set_defaults(func=cli_validate)

    args = parser.parse_args()
    print(args)
    args.func(args)

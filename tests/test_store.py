import io
import json
from pathlib import Path
import os
import tempfile
from typing import Optional
import unittest

from mongoengine import connect, disconnect
from mongomock.gridfs import enable_gridfs_integration
from typing import IO, Tuple

from killerdroid.packer.io import FsStore, MongoStore
from killerdroid.models import Apk, PackerApk, to_sha256, DoesNotExist


enable_gridfs_integration()


def create_content(content: Optional[IO] = b"1234567890") -> Tuple[IO, str]:
    content = io.BytesIO(content)
    sha256 = to_sha256(content)
    content.seek(0)
    return content, sha256


def create_content_with_image(
    content: Optional[IO] = b"1234567890",
) -> Tuple[IO, str, IO]:
    from PIL import Image

    content, sha256 = create_content(content=content)
    img = io.BytesIO()
    image = Image.new("RGBA", size=(256, 256), color=(155, 0, 0))
    image.save(img, "png")
    img.seek(0)
    return content, sha256, img


class TestMongoStore(unittest.TestCase):
    def setUp(self):
        self.db = connect("mongoenginetest", host="mongomock://localhost/test")

    def tearDown(self):
        self.db.drop_database("test")
        disconnect()

    def test_save_apk(self):
        content, sha256 = create_content()
        store = MongoStore()
        store.save_apk("test0", content)

        apk = Apk.objects.get(name="test0")
        self.assertIsNotNone(apk)
        apk = Apk.objects.get(sha256=sha256)
        self.assertIsNotNone(apk)

    def test_save_packer_apk(self):
        host, host_sha256 = create_content(b"1234567890")
        guest, guest_sha256 = create_content(b"azertyuiop")
        packed, packed_sha256 = create_content(b"1a2z3e")
        store = MongoStore()

        store.save_apk("host", host)
        store.save_apk("guest", guest)
        store.save_packer_apk(
            host_sha256, guest_sha256, "hash", {"config": "test"}, packed, ""
        )

        papk = PackerApk.objects.get(sha256=packed_sha256)
        self.assertIsNotNone(papk)

    def test_save_packer_apk_missing_guest(self):
        host, host_sha256 = create_content(b"1234567890")
        guest, guest_sha256 = create_content(b"azertyuiop")
        packed, packed_sha256 = create_content(b"1a2z3e")
        store = MongoStore()

        store.save_apk("host", host)

        with self.assertRaises(DoesNotExist):
            store.save_packer_apk(host_sha256, guest_sha256, "hash", {}, packed, "")

    def test_pull_apk(self):
        content, sha256 = create_content()
        with tempfile.TemporaryDirectory() as t:
            store = MongoStore()
            store.save_apk("test1", content)

            store.pull(sha256, output_dir=Path(t))
            # checking the file in the cache dir
            cached = Path(t) / f"{sha256}.apk"
            self.assertTrue(cached.exists())

    def test_save_screenshot(self):
        content, sha256, image = create_content_with_image()
        store = MongoStore()
        store.save_apk("test1", content)
        # save the screenshot
        store.save_screenshot(sha256, image, test=True)
        with tempfile.TemporaryDirectory() as t:
            screenshot_path = store.pull_screenshot(sha256, output_dir=Path(t))
            # TODO and should be equal to image
            self.assertTrue(screenshot_path.resolve().exists())

    def test_iter(self):
        a, a_sha256 = create_content(b"1234567890")
        b, b_sha256 = create_content(b"azertyuiop")
        store = MongoStore()
        store.save_apk("test0", a)
        store.save_apk("test1", b)
        all_shas = list(store.iter())
        self.assertCountEqual([a_sha256, b_sha256], all_shas)

    def test_update_metadata_api(self):
        a, sha256 = create_content()
        store = MongoStore()
        store.save_apk("test0", a)
        store.update_metadata(sha256, plop="plip")
        metadata = store.get_metadata(sha256)
        self.assertEqual("plip", metadata["plop"])

    def test_get_metadata_packed(self):
        a, a_sha256 = create_content(b"1234567890")
        b, b_sha256 = create_content(b"azertyuiop")
        c, c_sha256 = create_content(b"1a2b3c")
        store = MongoStore()
        store.save_apk("test0", a)
        store.save_apk("test1", b)
        store.save_packer_apk(
            host_sha256=a_sha256,
            guest_sha256=b_sha256,
            content=c,
            config_hash="a",
            config={"c": "d"},
            cmd="",
        )
        metadata = store.get_metadata(c_sha256)
        self.assertEqual(a_sha256, metadata["host_sha256"])
        self.assertEqual(b_sha256, metadata["guest_sha256"])
        self.assertEqual("__packer_apk__", metadata["name"])
        self.assertDictEqual({"c": "d"}, metadata["config"])


class TestFsStore(unittest.TestCase):
    def test_save_apk(self):
        content, sha256 = create_content()
        with tempfile.TemporaryDirectory() as t:
            store = FsStore(directory=Path(t))
            store.save_apk("test0", content)

            path = Path(t) / sha256 / f"{sha256}.apk"
            self.assertTrue(path.exists() and path.is_file())

            metadata = Path(t) / sha256 / f"{sha256}.apk.json"
            self.assertTrue(metadata.exists() and path.is_file())

    def test_pull(self):
        content, sha256 = create_content()
        with tempfile.TemporaryDirectory() as t:
            store = FsStore(directory=Path(t))
            store.save_apk("test0", content)

            with tempfile.TemporaryDirectory() as dest:
                apk_path = store.pull(sha256, output_dir=Path(dest))
                self.assertTrue(apk_path.exists() and apk_path.is_file())
                # nothing bad should happen
                store.pull(sha256, output_dir=Path(dest))

    def test_save_packer_apk(self):
        host, host_sha256 = create_content(b"1234567890")
        guest, guest_sha256 = create_content(b"azertyuiop")
        packed, packed_sha256 = create_content(b"1a2z3e")
        with tempfile.TemporaryDirectory() as t:
            store = FsStore(Path(t))

            store.save_apk("host", host)
            store.save_apk("guest", guest)
            store.save_packer_apk(host_sha256, guest_sha256, "hash", {}, packed, "")

            papk = Path(t) / packed_sha256 / f"{packed_sha256}.apk"
            self.assertTrue(papk.exists() and papk.is_file())

            metadata_path = Path(t) / packed_sha256 / f"{packed_sha256}.apk.json"
            self.assertTrue(metadata_path.exists() and metadata_path.is_file())

            metadata = json.load(metadata_path.open("r"))
            self.assertEqual(guest_sha256, metadata["guest_sha256"])

    def test_save_screenshot(self):
        content, sha256, image = create_content_with_image()
        with tempfile.TemporaryDirectory() as t:
            store = FsStore(Path(t))
            store.save_apk("test1", content)
            # save the screenshot
            store.save_screenshot(sha256, image, test=True)

            png = Path(t) / sha256 / f"{sha256}.png"
            self.assertTrue(png.exists() and png.is_file())

            metadata_path = Path(t) / sha256 / f"{sha256}.png.json"
            self.assertTrue(metadata_path.exists() and metadata_path.is_file())

            screenshot_path = store.pull_screenshot(sha256, output_dir=Path(t))

            # TODO and should be equal to image
            self.assertTrue(screenshot_path.resolve().exists())

    def test_update_metadata(self):
        with tempfile.TemporaryDirectory() as t:
            store = FsStore(Path(t))

            metadata_path = Path(t) / "a" / "1234.json"
            store._update_metadata(metadata_path, a=1)
            store._update_metadata(metadata_path, b=2)
            store._update_metadata(metadata_path, a=3)

            self.assertTrue(metadata_path.exists() and metadata_path.is_file())
            metadata = json.load(metadata_path.open("r"))
            self.assertEqual(3, metadata["a"])
            self.assertEqual(2, metadata["b"])

    def test_iter(self):
        a, a_sha256 = create_content(b"1234567890")
        b, b_sha256 = create_content(b"azertyuiop")
        with tempfile.TemporaryDirectory() as tmp:
            store = FsStore(Path(tmp))
            store.save_apk("test0", a)
            store.save_apk("test1", b)
            all_shas = list(store.iter())
            self.assertCountEqual([a_sha256, b_sha256], all_shas)

    def test_update_metadata_api(self):
        a, sha256 = create_content()
        with tempfile.TemporaryDirectory() as tmp:
            store = FsStore(Path(tmp))
            store.save_apk("test0", a)
            store.update_metadata(sha256, plop="plip")

            metadata = store.get_metadata(sha256)
            self.assertEqual("plip", metadata["plop"])

    def test_get_metadata_packed(self):
        a, a_sha256 = create_content(b"1234567890")
        b, b_sha256 = create_content(b"azertyuiop")
        c, c_sha256 = create_content(b"1a2b3c")
        with tempfile.TemporaryDirectory() as tmp:
            store = FsStore(Path(tmp))
            store.save_apk("test0", a)
            store.save_apk("test1", b)
            store.save_packer_apk(
                host_sha256=a_sha256,
                guest_sha256=b_sha256,
                content=c,
                config_hash="a",
                config={"c": "d"},
                cmd="",
            )
            metadata = store.get_metadata(c_sha256)
            self.assertEqual(a_sha256, metadata["host_sha256"])
            self.assertEqual(b_sha256, metadata["guest_sha256"])
            self.assertEqual("__packer_apk__", metadata["name"])
            self.assertDictEqual({"c": "d"}, metadata["config"])
